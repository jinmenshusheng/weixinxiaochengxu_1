const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  return  month + "月" + day + "日"
}

const formatDay = dates =>{
  const _day = new Array('周日','周一','周二','周三','周四','周五','周六');
  const date = new Date(dates);
  date.setDate(date.getDate());
  const day = date.getDay();
  return _day[day];
}

const formateSole = () =>{
  const timeNow = new Date();
  const hours = timeNow.getHours();
  let text = ``;
  if (hours >= 23 && hours <= 1) {
    text = `子时`;
  } else if (hours > 1 && hours <= 3) {
    text = `丑时`;
  } else if (hours > 3 && hours <= 5) {
    text = `寅时`;
  } else if (hours > 5 && hours <= 7) {
    text = `卯时`;
  } else if (hours > 7 && hours <= 9) {
    text = `辰时`;
  } else if (hours > 9 && hours <= 11) {
    text = `巳时`;
  } else if (hours > 11 && hours <= 13) {
    text = `午时`;
  } else if (hours > 13 && hours <= 15) {
    text = `未时`;
  } else if (hours > 15 && hours <= 17) {
    text = `申时`;
  } else if (hours > 17 && hours <= 19) {
    text = `酉时`;
  } else if (hours > 19 && hours <= 21) {
    text = `戌时`;
  } else if (hours > 21 && hours <= 23) {
    text = `亥时`;
  }
  
  return text;
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

module.exports = {
  formatTime:formatTime,
  formatDay:formatDay,
  formateSole:formateSole
}
