var app = getApp();
var api = require('../../../utils/api')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [], isIcon: {}, scrollTop: 0,
    result: [],
    images: {},
    modalName:null,
    resultData: null,
    img: '',
    modalName: '',
    modalTitle: null,
    modalContent: null,
    minioUrl:null,
    logoName:null,
    apiSource:'百度AI-logo识别'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
  //获取输入的内容
  getlogoName: function (e) {
    this.setData({
      logoName: e.detail.value.replace(/\s+/g, '')
    })
  },
  //添加到后台
  toAddLogo:function () {
    var that = this;
    wx.showLoading({
      title: "提交中...",
      mask: true
    }),
        api.addLogoRequest(app.globalData.userId,that.data.logoName,that.data.minioUrl, {
          success(result) {
            wx.hideLoading();
            if (result.code == 200) {
              that.setData({
                minioUrl: null,
                logoName:null,
                img:null,
                resultData:null
              })
              that.hideModal();
              wx.showModal({
                content: result.msg_zh,
                showCancel: false,
                confirmText: '好的'
              })
            } else {
              if (result.code == 87014) {
                wx.hideLoading();
                wx.showModal({
                  content: '存在敏感内容，请重新输入',
                  showCancel: false,
                  confirmText: '明白了'
                })
                that.setData({
                  logoName:null
                })
              } else {
                wx.hideLoading();
                wx.showModal({
                  content: result.msg_zh,
                  showCancel: false,
                  confirmText: '明白了'
                })
              }
            }
          }
        })
  },
  //打开详情介绍
  showDetail: function (e) {
    var that = this;
    var description = e.target.dataset.description;
    var name = e.target.dataset.name;
    if (description == undefined) {
      wx.showToast({
        title: '暂无介绍内容',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    } else {
      if (description.length > 0) {
        wx.showModal({
          title: name,
          content: description,
          showCancel: false,
          confirmText: '关闭介绍',
          confirmColor: '#02A0E7'
        })
      } else {
        wx.showToast({
          title: '暂无介绍内容',
          icon: 'none',
          duration: 2000,
          mask: true
        })
      }
    }
  },
  //从聊天页面选择图片
  chooseMessage:function(){
    var that = this;
    wx.chooseMessageFile({
      count: 1,
      sizeType: ['compressed'],
      type:'image',
      success(res){
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.setData({
            img: res.tempFiles[0].path
          })
          that.icrDetect(res.tempFiles[0].path);
        }
      }
    })
  },
  //收录LOGO 内容输入弹窗
  addLogo:function () {
    var that = this;
    that.setData({
      modalName:'addLogoModal'
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  //请求方法
  chooseImageFile: function () {
    var that = this
    var takephonewidth
    var takephoneheight
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        wx.getImageInfo({
          src: res.tempFilePaths[0],
          success(res) {
            takephonewidth = res.width,
                takephoneheight = res.height
          }
        })
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.icrDetect(res.tempFilePaths[0]);
        }
      },
    })
  },
  //LOGO识别
  icrDetect(file){
    var that = this;
    that.setData({
      img: file
    }),
        wx.showLoading({
          title: "LOGO检索中...",
          mask: true
        }),
        api.icrRequest(file, app.globalData.userId, 'logo', {
          success(result) {
            var resultJ = JSON.parse(result)
            wx.hideLoading();
            if (resultJ.code == 200) {
              that.setData({
                resultData: resultJ.result,
                minioUrl:resultJ.minio_url
              })
            } else {
              if (resultJ.code == 87014) {
                wx.hideLoading();
                wx.showModal({
                  content: '存在敏感内容，请更换图片',
                  showCancel: false,
                  confirmText: '明白了'
                })
                that.setData({
                  img: null
                })
              } else {
                wx.hideLoading();
                wx.showModal({
                  content: resultJ.msg_zh,
                  showCancel: false,
                  confirmText: '明白了'
                })
              }
            }
          }
        })
  },
  onLoad: function () {
  },
  /**
   * 点击查看图片，可以进行保存
   */
  preview(e) {
    var that = this;
    wx.previewImage({
      urls: [that.data.img],
      current: that.data.img
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
