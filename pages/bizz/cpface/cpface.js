var app = getApp();
var api = require('../../../utils/api')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [], isIcon: {}, scrollTop: 0,apiSource:'百度AI-人脸检测-作者加工',
    result: [],
    fileKeyA:null,
    fileKeyB:null,
    fileA:null,
    fileB:null,
    cpFace:null,
    images: {},
    resultData: null,
    img: '',
    imgA: '',
    imgB: '',
    modalName: '',
    modalTitle: null,
    modalContent: null
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var timestamp = Date.parse(new Date());
    var random =  app.globalData.userId
    this.setData({
      list: [{icon: '_icon-home-o', num: 1}],
      isIcon: {down: 'cicon-unfold-less', top: 'cicon-eject', up: 'cicon-unfold-more'}
    })
    this.setData({
      fileKeyA:'A'+timestamp+random,
      fileKeyB:'B'+timestamp+random
    })
  },
  tapToolsBar(e) {
    var pageNum = e.detail.item.num;
    if(pageNum==1){
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  //从聊天页面选择图片
  chooseMessageA:function(){
    var that = this;
    wx.chooseMessageFile({
      count: 1,
      sizeType: ['compressed'],
      type:'image',
      success(res){
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.setData({
            imgA: res.tempFiles[0].path
          })
          that.uploadFile(res.tempFiles[0].path,that.data.fileKeyA);
        }
      }
    })
  },
  //从聊天页面选择图片
  chooseMessageB:function(){
    var that = this;
    wx.chooseMessageFile({
      count: 1,
      sizeType: ['compressed'],
      type:'image',
      success(res){
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.setData({
            imgB: res.tempFiles[0].path
          })
          that.uploadFile(res.tempFiles[0].path,that.data.fileKeyB);
        }
      }
    })
  },
  //相册-相机选择图片
  chooseImageFileA: function () {
    var that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.setData({
            imgA: res.tempFilePaths[0]
          })
          that.uploadFile(res.tempFilePaths[0],that.data.fileKeyA);
        }
      },
    })
  },
  //相册-相机选择图片
  chooseImageFileB: function () {
    var that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.setData({
            imgB: res.tempFilePaths[0]
          })
          that.uploadFile(that.data.imgB,that.data.fileKeyB);
        }
      },
    })
  },
  //上传图片
  uploadFile(file,key){
    var that = this;
    var keyStart = key.indexOf("A");
    if(keyStart==0){
      wx.showLoading({
        title: "左脸准备中...",
        mask: true
      })
    }else{
      wx.showLoading({
        title: "右脸准备中...",
        mask: true
      })
    }
    api.cpFaceRequest(file,key,app.globalData.userId, {
      success(result) {
        var resultJ = JSON.parse(result)
        wx.hideLoading();
        if (resultJ.code == 200) {
          if(keyStart==0){
            that.setData({
              fileA: resultJ.data_flower.file_key_a
            })
          }else{
            that.setData({
              fileB: resultJ.data_flower.file_key_b
            })
          }
          console.info(resultJ)
        } else {
          if (resultJ.code == 87014) {
            wx.hideLoading();
            wx.showModal({
              content: '存在敏感内容，请更换图片',
              showCancel: false,
              confirmText: '明白了'
            })
            if(keyStart==0){
              that.setData({
                imgA: null
              })
            }else{
              that.setData({
                imgB: null
              })
            }
          } else {
            wx.hideLoading();
            wx.showModal({
              content: resultJ.msg_zh,
              showCancel: false,
              confirmText: '明白了'
            })
          }
        }
      }
    })
  },
  //进行拼脸
  uploadMerge:function () {
    var that = this;
    if(null!=that.data.fileA && null!=that.data.fileB ){
      wx.showLoading({
        title: "情侣拼脸中...",
        mask: true
      })
      api.cpFaceMergeRequest(app.globalData.userId,that.data.fileA,that.data.fileB, {
        success(result) {
          var resultJ = result
          wx.hideLoading();
          if (resultJ.code == 200) {
            that.setData({
              cpFace:'data:image/jpg;base64,' + resultJ.data_flower.image_base64
            })
          } else {
            if (resultJ.code == 87014) {
              wx.hideLoading();
              wx.showModal({
                content: '存在敏感内容，请更换图片',
                showCancel: false,
                confirmText: '明白了'
              })
            } else {
              wx.hideLoading();
              wx.showModal({
                content: resultJ.msg_zh,
                showCancel: false,
                confirmText: '明白了'
              })
            }
          }
        }
      })
    } else {
      wx.showModal({
        content: '请确保两种图片都已上传',
        showCancel: false,
        confirmText: '好的'
      })
    }
  },
  /**
   * 重新拼脸
   */
  againMerge(){
    var that = this;
    var timestamp = Date.parse(new Date());
    var random =  app.globalData.userId
    that.setData({
      fileKeyA:'A'+timestamp+random,
      fileKeyB:'B'+timestamp+random,
      imgA:null,
      imgB:null,
      cpFace:null,
      fileA:null,
      fileB:null
    })
  },
  /**
   * 点击查看图片，可以进行保存
   */
  preview(e) {
    var that = this;
    if (null == that.data.cpFace || that.data.cpFace == '') {
      wx.showModal({
        title: '温馨提示',
        content: '未选择任何图片',
        showCancel: false,
        confirmText: '好的'
      })
    } else {
      wx.previewImage({
        urls: [that.data.cpFace],
        current: that.data.cpFace
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
