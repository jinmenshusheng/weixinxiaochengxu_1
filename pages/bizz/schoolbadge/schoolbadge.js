var app = getApp();
var api = require('../../../utils/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    apiSource:'百度AI-EasyDL',
    list: [], isIcon: {}, scrollTop: 0,
    result: [],
    images: {},
    schoolbadge: null,
    img: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    var that = this;
    that.setData({
      list: [{icon: '_icon-home-o', num: 1}],
      isIcon: {down: 'cicon-unfold-less', top: 'cicon-eject', up: 'cicon-unfold-more'}
    })
  },
  tapToolsBar(e) {
    var pageNum = e.detail.item.num;
    if(pageNum==1){
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  //从聊天页面选择图片
  chooseMessage:function(){
    var that = this;
    wx.chooseMessageFile({
      count: 1,
      sizeType: ['compressed'],
      type:'image',
      success(res){
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.setData({
            img: res.tempFiles[0].path
          })
          that.generalDetect(res.tempFiles[0].path);
        }
      }
    })
  },
  //请求方法
  chooseImageFile: function () {
    var that = this
    var takephonewidth
    var takephoneheight
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        wx.getImageInfo({
          src: res.tempFilePaths[0],
          success(res) {
            takephonewidth = res.width,
                takephoneheight = res.height
          }
        })
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.generalDetect(res.tempFilePaths[0]);
        }

      },
    })
  },
  //通用识别
  generalDetect(file){
    var that = this;
    that.setData({
      img: file
    }),
        wx.showLoading({
          title: "校徽识别中...",
          mask: true
        }),
        api.generalRequest(file, app.globalData.userId, api.schoolbadge_url, {
          success(result) {
            var resultJ = JSON.parse(result)
            wx.hideLoading();
            if (resultJ.code == 200) {
              that.setData({
                schoolbadge: resultJ.schoolBadgeData
              })
            } else {
              if (resultJ.code == 87014) {
                wx.hideLoading();
                wx.showModal({
                  content: '存在敏感内容，请更换图片',
                  showCancel: false,
                  confirmText: '明白了'
                })
                that.setData({
                  img: null,
                  schoolbadge:null
                })
              } else {
                wx.hideLoading();
                wx.showModal({
                  content: resultJ.msg_zh,
                  showCancel: false,
                  confirmText: '明白了'
                })
              }
            }
          }
        })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
