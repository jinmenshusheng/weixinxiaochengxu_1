Component({
    data: {
        colorName: ''
    },
    options: {
        addGlobalClass: true
    },
    properties: {
        data: {
            type: Object,
            optionalTypes: Array,
            value: {}
        }
    },
    lifetimes: {
        attached() {
            this.setData({
                colorName: this.getColor()
            });
        }
    },
    methods:{
        //随机生成库内颜色名
        getColor() {
            let colorArr = ['yellow', 'orange', 'red', 'pink', 'mauve', 'purple', 'blue', 'cyan', 'green', 'olive', 'grey', 'brown'];
            return colorArr[Math.floor(Math.random() * colorArr.length)]
        },
        toNavigate:function(e){
            var url = e.currentTarget.dataset.url;
            var jump = e.currentTarget.dataset.jump;
            if(jump==true){
                wx.navigateTo({
                    url: url
                })
            }else{
                wx.showToast({
                    title: '升级改造中,敬请期待',
                    icon: 'none',
                    mask: true
                })
            }
        },
    },
})
