const app = getApp();
var api = require('../../../utils/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [], isIcon: {}, scrollTop: 0,
    gridList: [{
      icon: '../../../static/images/xtools/invoice.png',
      color: 'red',
      badge: 120,
      id: 1,
      name: '发票行程单合并'
    }]
  },
  //页面跳转
  toPage: function (event) {
    var route = event.currentTarget.id;
    if (route == 1) {
      wx.navigateTo({
        url: '/pages/bizz/pdfmerge/pdfmerge',
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      list: [{icon: '_icon-home-o', num: 1}],
      isIcon: {down: 'cicon-unfold-less', top: 'cicon-eject', up: 'cicon-unfold-more'}
    })
  },
  tapToolsBar(e) {
    var pageNum = e.detail.item.num;
    if(pageNum==1){
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
