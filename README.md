[![Version](https://img.shields.io/badge/version-3.0.0beta-brightgreen.svg)](https://gitee.com/xshuai/weixinxiaochengxu/)
[![作者](https://img.shields.io/badge/%E4%BD%9C%E8%80%85-%E5%B0%8F%E5%B8%85%E4%B8%B6-7AD6FD.svg)](https://www.ydxiaoshuai.cn/)

# 微信小程序-微信端源代码

### 关注码小帅获取最新功能更新

![码小帅](https://images.gitee.com/uploads/images/2021/0518/164321_b70506da_131538.jpeg "码小帅.jpg")

联系我(Q/X):783021975

 
# 使用说明
#### 下载源码 用微信开发工具打开  在 **utils-api.js** 替换自己的域名相关信息即可。
#### 后台代码:[https://gitee.com/xshuai/xai](https://gitee.com/xshuai/xai)
#### 后台页面代码:[https://gitee.com/xshuai/xaiboot-vue](https://gitee.com/xshuai/xaiboot-vue)



```
       ├── config                                //ColorUI基础配置文件
       │       └── ColorUI.js                    //底部导航配置 
       │       └── mp-sdk.js                     //环境配置-非必须       
       ├── mp-cu                                 //组件和样式
       ├── pages                               
       │       └── aifun                          //AI趣玩主页面         
       │       └── aiindex                        //小程序首页涉及的各个模块的页面       
       │       └── ailab                          //AILab            
       │       └── ailife                         //AI生活主页面    
       │       └── bizz                           //具体功能模块页面
       │       └── index                          //小程序首页  
       │       └── mine                           //我的页面         
       │       └── set                            //设置页面
       ├── utils                                
       │       └── api.js                        //全部的接口url以及封装的接口调用方法在这里配置
       │       └── util.js                       //暂时没用      
       │       └── weather.js                    //天气查询相关工具类 
       ├── app.js                                //全局js配置文件
       ├── app.json                              //全局配置json文件
       ├── app.wxss                              //全局wxss文件
       └── project.config.json                   //工具配置(个性化配置)
```

![新版首页](index.jpg)